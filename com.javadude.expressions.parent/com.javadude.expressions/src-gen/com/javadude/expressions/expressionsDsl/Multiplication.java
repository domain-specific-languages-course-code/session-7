/**
 * generated by Xtext 2.14.0
 */
package com.javadude.expressions.expressionsDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.javadude.expressions.expressionsDsl.Multiplication#getOp1 <em>Op1</em>}</li>
 *   <li>{@link com.javadude.expressions.expressionsDsl.Multiplication#getOp <em>Op</em>}</li>
 *   <li>{@link com.javadude.expressions.expressionsDsl.Multiplication#getOp2 <em>Op2</em>}</li>
 * </ul>
 *
 * @see com.javadude.expressions.expressionsDsl.ExpressionsDslPackage#getMultiplication()
 * @model
 * @generated
 */
public interface Multiplication extends Expression
{
  /**
   * Returns the value of the '<em><b>Op1</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op1</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op1</em>' containment reference.
   * @see #setOp1(Expression)
   * @see com.javadude.expressions.expressionsDsl.ExpressionsDslPackage#getMultiplication_Op1()
   * @model containment="true"
   * @generated
   */
  Expression getOp1();

  /**
   * Sets the value of the '{@link com.javadude.expressions.expressionsDsl.Multiplication#getOp1 <em>Op1</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op1</em>' containment reference.
   * @see #getOp1()
   * @generated
   */
  void setOp1(Expression value);

  /**
   * Returns the value of the '<em><b>Op</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op</em>' attribute.
   * @see #setOp(String)
   * @see com.javadude.expressions.expressionsDsl.ExpressionsDslPackage#getMultiplication_Op()
   * @model
   * @generated
   */
  String getOp();

  /**
   * Sets the value of the '{@link com.javadude.expressions.expressionsDsl.Multiplication#getOp <em>Op</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op</em>' attribute.
   * @see #getOp()
   * @generated
   */
  void setOp(String value);

  /**
   * Returns the value of the '<em><b>Op2</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Op2</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Op2</em>' containment reference.
   * @see #setOp2(Expression)
   * @see com.javadude.expressions.expressionsDsl.ExpressionsDslPackage#getMultiplication_Op2()
   * @model containment="true"
   * @generated
   */
  Expression getOp2();

  /**
   * Sets the value of the '{@link com.javadude.expressions.expressionsDsl.Multiplication#getOp2 <em>Op2</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Op2</em>' containment reference.
   * @see #getOp2()
   * @generated
   */
  void setOp2(Expression value);

} // Multiplication
