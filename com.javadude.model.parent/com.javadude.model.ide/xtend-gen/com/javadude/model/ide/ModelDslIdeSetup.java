/**
 * generated by Xtext 2.14.0
 */
package com.javadude.model.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.javadude.model.ModelDslRuntimeModule;
import com.javadude.model.ModelDslStandaloneSetup;
import com.javadude.model.ide.ModelDslIdeModule;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
@SuppressWarnings("all")
public class ModelDslIdeSetup extends ModelDslStandaloneSetup {
  @Override
  public Injector createInjector() {
    ModelDslRuntimeModule _modelDslRuntimeModule = new ModelDslRuntimeModule();
    ModelDslIdeModule _modelDslIdeModule = new ModelDslIdeModule();
    return Guice.createInjector(Modules2.mixin(_modelDslRuntimeModule, _modelDslIdeModule));
  }
}
