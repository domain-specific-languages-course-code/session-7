package com.javadude.model.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import com.javadude.model.services.ModelDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalModelDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "'.'", "'class'", "'{'", "'}'", "':'", "'int'", "'string'", "'list'", "'of'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalModelDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalModelDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalModelDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalModelDsl.g"; }


    	private ModelDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(ModelDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalModelDsl.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalModelDsl.g:54:1: ( ruleModel EOF )
            // InternalModelDsl.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalModelDsl.g:62:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:66:2: ( ( ( rule__Model__Group__0 ) ) )
            // InternalModelDsl.g:67:2: ( ( rule__Model__Group__0 ) )
            {
            // InternalModelDsl.g:67:2: ( ( rule__Model__Group__0 ) )
            // InternalModelDsl.g:68:3: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // InternalModelDsl.g:69:3: ( rule__Model__Group__0 )
            // InternalModelDsl.g:69:4: rule__Model__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleqid"
    // InternalModelDsl.g:78:1: entryRuleqid : ruleqid EOF ;
    public final void entryRuleqid() throws RecognitionException {
        try {
            // InternalModelDsl.g:79:1: ( ruleqid EOF )
            // InternalModelDsl.g:80:1: ruleqid EOF
            {
             before(grammarAccess.getQidRule()); 
            pushFollow(FOLLOW_1);
            ruleqid();

            state._fsp--;

             after(grammarAccess.getQidRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleqid"


    // $ANTLR start "ruleqid"
    // InternalModelDsl.g:87:1: ruleqid : ( ( rule__Qid__Group__0 ) ) ;
    public final void ruleqid() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:91:2: ( ( ( rule__Qid__Group__0 ) ) )
            // InternalModelDsl.g:92:2: ( ( rule__Qid__Group__0 ) )
            {
            // InternalModelDsl.g:92:2: ( ( rule__Qid__Group__0 ) )
            // InternalModelDsl.g:93:3: ( rule__Qid__Group__0 )
            {
             before(grammarAccess.getQidAccess().getGroup()); 
            // InternalModelDsl.g:94:3: ( rule__Qid__Group__0 )
            // InternalModelDsl.g:94:4: rule__Qid__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Qid__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQidAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleqid"


    // $ANTLR start "entryRuleClassDecl"
    // InternalModelDsl.g:103:1: entryRuleClassDecl : ruleClassDecl EOF ;
    public final void entryRuleClassDecl() throws RecognitionException {
        try {
            // InternalModelDsl.g:104:1: ( ruleClassDecl EOF )
            // InternalModelDsl.g:105:1: ruleClassDecl EOF
            {
             before(grammarAccess.getClassDeclRule()); 
            pushFollow(FOLLOW_1);
            ruleClassDecl();

            state._fsp--;

             after(grammarAccess.getClassDeclRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClassDecl"


    // $ANTLR start "ruleClassDecl"
    // InternalModelDsl.g:112:1: ruleClassDecl : ( ( rule__ClassDecl__Group__0 ) ) ;
    public final void ruleClassDecl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:116:2: ( ( ( rule__ClassDecl__Group__0 ) ) )
            // InternalModelDsl.g:117:2: ( ( rule__ClassDecl__Group__0 ) )
            {
            // InternalModelDsl.g:117:2: ( ( rule__ClassDecl__Group__0 ) )
            // InternalModelDsl.g:118:3: ( rule__ClassDecl__Group__0 )
            {
             before(grammarAccess.getClassDeclAccess().getGroup()); 
            // InternalModelDsl.g:119:3: ( rule__ClassDecl__Group__0 )
            // InternalModelDsl.g:119:4: rule__ClassDecl__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ClassDecl__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getClassDeclAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClassDecl"


    // $ANTLR start "entryRulePropertyDecl"
    // InternalModelDsl.g:128:1: entryRulePropertyDecl : rulePropertyDecl EOF ;
    public final void entryRulePropertyDecl() throws RecognitionException {
        try {
            // InternalModelDsl.g:129:1: ( rulePropertyDecl EOF )
            // InternalModelDsl.g:130:1: rulePropertyDecl EOF
            {
             before(grammarAccess.getPropertyDeclRule()); 
            pushFollow(FOLLOW_1);
            rulePropertyDecl();

            state._fsp--;

             after(grammarAccess.getPropertyDeclRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePropertyDecl"


    // $ANTLR start "rulePropertyDecl"
    // InternalModelDsl.g:137:1: rulePropertyDecl : ( ( rule__PropertyDecl__Group__0 ) ) ;
    public final void rulePropertyDecl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:141:2: ( ( ( rule__PropertyDecl__Group__0 ) ) )
            // InternalModelDsl.g:142:2: ( ( rule__PropertyDecl__Group__0 ) )
            {
            // InternalModelDsl.g:142:2: ( ( rule__PropertyDecl__Group__0 ) )
            // InternalModelDsl.g:143:3: ( rule__PropertyDecl__Group__0 )
            {
             before(grammarAccess.getPropertyDeclAccess().getGroup()); 
            // InternalModelDsl.g:144:3: ( rule__PropertyDecl__Group__0 )
            // InternalModelDsl.g:144:4: rule__PropertyDecl__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PropertyDecl__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyDeclAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePropertyDecl"


    // $ANTLR start "entryRuleType"
    // InternalModelDsl.g:153:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalModelDsl.g:154:1: ( ruleType EOF )
            // InternalModelDsl.g:155:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalModelDsl.g:162:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:166:2: ( ( ( rule__Type__Alternatives ) ) )
            // InternalModelDsl.g:167:2: ( ( rule__Type__Alternatives ) )
            {
            // InternalModelDsl.g:167:2: ( ( rule__Type__Alternatives ) )
            // InternalModelDsl.g:168:3: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // InternalModelDsl.g:169:3: ( rule__Type__Alternatives )
            // InternalModelDsl.g:169:4: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleIntType"
    // InternalModelDsl.g:178:1: entryRuleIntType : ruleIntType EOF ;
    public final void entryRuleIntType() throws RecognitionException {
        try {
            // InternalModelDsl.g:179:1: ( ruleIntType EOF )
            // InternalModelDsl.g:180:1: ruleIntType EOF
            {
             before(grammarAccess.getIntTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleIntType();

            state._fsp--;

             after(grammarAccess.getIntTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntType"


    // $ANTLR start "ruleIntType"
    // InternalModelDsl.g:187:1: ruleIntType : ( ( rule__IntType__Group__0 ) ) ;
    public final void ruleIntType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:191:2: ( ( ( rule__IntType__Group__0 ) ) )
            // InternalModelDsl.g:192:2: ( ( rule__IntType__Group__0 ) )
            {
            // InternalModelDsl.g:192:2: ( ( rule__IntType__Group__0 ) )
            // InternalModelDsl.g:193:3: ( rule__IntType__Group__0 )
            {
             before(grammarAccess.getIntTypeAccess().getGroup()); 
            // InternalModelDsl.g:194:3: ( rule__IntType__Group__0 )
            // InternalModelDsl.g:194:4: rule__IntType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IntType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIntTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntType"


    // $ANTLR start "entryRuleStringType"
    // InternalModelDsl.g:203:1: entryRuleStringType : ruleStringType EOF ;
    public final void entryRuleStringType() throws RecognitionException {
        try {
            // InternalModelDsl.g:204:1: ( ruleStringType EOF )
            // InternalModelDsl.g:205:1: ruleStringType EOF
            {
             before(grammarAccess.getStringTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleStringType();

            state._fsp--;

             after(grammarAccess.getStringTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringType"


    // $ANTLR start "ruleStringType"
    // InternalModelDsl.g:212:1: ruleStringType : ( ( rule__StringType__Group__0 ) ) ;
    public final void ruleStringType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:216:2: ( ( ( rule__StringType__Group__0 ) ) )
            // InternalModelDsl.g:217:2: ( ( rule__StringType__Group__0 ) )
            {
            // InternalModelDsl.g:217:2: ( ( rule__StringType__Group__0 ) )
            // InternalModelDsl.g:218:3: ( rule__StringType__Group__0 )
            {
             before(grammarAccess.getStringTypeAccess().getGroup()); 
            // InternalModelDsl.g:219:3: ( rule__StringType__Group__0 )
            // InternalModelDsl.g:219:4: rule__StringType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StringType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStringTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringType"


    // $ANTLR start "entryRuleListType"
    // InternalModelDsl.g:228:1: entryRuleListType : ruleListType EOF ;
    public final void entryRuleListType() throws RecognitionException {
        try {
            // InternalModelDsl.g:229:1: ( ruleListType EOF )
            // InternalModelDsl.g:230:1: ruleListType EOF
            {
             before(grammarAccess.getListTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleListType();

            state._fsp--;

             after(grammarAccess.getListTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleListType"


    // $ANTLR start "ruleListType"
    // InternalModelDsl.g:237:1: ruleListType : ( ( rule__ListType__Group__0 ) ) ;
    public final void ruleListType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:241:2: ( ( ( rule__ListType__Group__0 ) ) )
            // InternalModelDsl.g:242:2: ( ( rule__ListType__Group__0 ) )
            {
            // InternalModelDsl.g:242:2: ( ( rule__ListType__Group__0 ) )
            // InternalModelDsl.g:243:3: ( rule__ListType__Group__0 )
            {
             before(grammarAccess.getListTypeAccess().getGroup()); 
            // InternalModelDsl.g:244:3: ( rule__ListType__Group__0 )
            // InternalModelDsl.g:244:4: rule__ListType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ListType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getListTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleListType"


    // $ANTLR start "entryRuleRefType"
    // InternalModelDsl.g:253:1: entryRuleRefType : ruleRefType EOF ;
    public final void entryRuleRefType() throws RecognitionException {
        try {
            // InternalModelDsl.g:254:1: ( ruleRefType EOF )
            // InternalModelDsl.g:255:1: ruleRefType EOF
            {
             before(grammarAccess.getRefTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleRefType();

            state._fsp--;

             after(grammarAccess.getRefTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRefType"


    // $ANTLR start "ruleRefType"
    // InternalModelDsl.g:262:1: ruleRefType : ( ( rule__RefType__RefAssignment ) ) ;
    public final void ruleRefType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:266:2: ( ( ( rule__RefType__RefAssignment ) ) )
            // InternalModelDsl.g:267:2: ( ( rule__RefType__RefAssignment ) )
            {
            // InternalModelDsl.g:267:2: ( ( rule__RefType__RefAssignment ) )
            // InternalModelDsl.g:268:3: ( rule__RefType__RefAssignment )
            {
             before(grammarAccess.getRefTypeAccess().getRefAssignment()); 
            // InternalModelDsl.g:269:3: ( rule__RefType__RefAssignment )
            // InternalModelDsl.g:269:4: rule__RefType__RefAssignment
            {
            pushFollow(FOLLOW_2);
            rule__RefType__RefAssignment();

            state._fsp--;


            }

             after(grammarAccess.getRefTypeAccess().getRefAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRefType"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalModelDsl.g:277:1: rule__Type__Alternatives : ( ( ruleIntType ) | ( ruleStringType ) | ( ruleListType ) | ( ruleRefType ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:281:1: ( ( ruleIntType ) | ( ruleStringType ) | ( ruleListType ) | ( ruleRefType ) )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt1=1;
                }
                break;
            case 18:
                {
                alt1=2;
                }
                break;
            case 19:
                {
                alt1=3;
                }
                break;
            case RULE_ID:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalModelDsl.g:282:2: ( ruleIntType )
                    {
                    // InternalModelDsl.g:282:2: ( ruleIntType )
                    // InternalModelDsl.g:283:3: ruleIntType
                    {
                     before(grammarAccess.getTypeAccess().getIntTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleIntType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getIntTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalModelDsl.g:288:2: ( ruleStringType )
                    {
                    // InternalModelDsl.g:288:2: ( ruleStringType )
                    // InternalModelDsl.g:289:3: ruleStringType
                    {
                     before(grammarAccess.getTypeAccess().getStringTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleStringType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getStringTypeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalModelDsl.g:294:2: ( ruleListType )
                    {
                    // InternalModelDsl.g:294:2: ( ruleListType )
                    // InternalModelDsl.g:295:3: ruleListType
                    {
                     before(grammarAccess.getTypeAccess().getListTypeParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleListType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getListTypeParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalModelDsl.g:300:2: ( ruleRefType )
                    {
                    // InternalModelDsl.g:300:2: ( ruleRefType )
                    // InternalModelDsl.g:301:3: ruleRefType
                    {
                     before(grammarAccess.getTypeAccess().getRefTypeParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleRefType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getRefTypeParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__Model__Group__0"
    // InternalModelDsl.g:310:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:314:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // InternalModelDsl.g:315:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // InternalModelDsl.g:322:1: rule__Model__Group__0__Impl : ( 'package' ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:326:1: ( ( 'package' ) )
            // InternalModelDsl.g:327:1: ( 'package' )
            {
            // InternalModelDsl.g:327:1: ( 'package' )
            // InternalModelDsl.g:328:2: 'package'
            {
             before(grammarAccess.getModelAccess().getPackageKeyword_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getModelAccess().getPackageKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // InternalModelDsl.g:337:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:341:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // InternalModelDsl.g:342:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // InternalModelDsl.g:349:1: rule__Model__Group__1__Impl : ( ( rule__Model__PkgAssignment_1 ) ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:353:1: ( ( ( rule__Model__PkgAssignment_1 ) ) )
            // InternalModelDsl.g:354:1: ( ( rule__Model__PkgAssignment_1 ) )
            {
            // InternalModelDsl.g:354:1: ( ( rule__Model__PkgAssignment_1 ) )
            // InternalModelDsl.g:355:2: ( rule__Model__PkgAssignment_1 )
            {
             before(grammarAccess.getModelAccess().getPkgAssignment_1()); 
            // InternalModelDsl.g:356:2: ( rule__Model__PkgAssignment_1 )
            // InternalModelDsl.g:356:3: rule__Model__PkgAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Model__PkgAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getPkgAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // InternalModelDsl.g:364:1: rule__Model__Group__2 : rule__Model__Group__2__Impl ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:368:1: ( rule__Model__Group__2__Impl )
            // InternalModelDsl.g:369:2: rule__Model__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Model__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // InternalModelDsl.g:375:1: rule__Model__Group__2__Impl : ( ( rule__Model__ClassesAssignment_2 )* ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:379:1: ( ( ( rule__Model__ClassesAssignment_2 )* ) )
            // InternalModelDsl.g:380:1: ( ( rule__Model__ClassesAssignment_2 )* )
            {
            // InternalModelDsl.g:380:1: ( ( rule__Model__ClassesAssignment_2 )* )
            // InternalModelDsl.g:381:2: ( rule__Model__ClassesAssignment_2 )*
            {
             before(grammarAccess.getModelAccess().getClassesAssignment_2()); 
            // InternalModelDsl.g:382:2: ( rule__Model__ClassesAssignment_2 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalModelDsl.g:382:3: rule__Model__ClassesAssignment_2
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Model__ClassesAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getClassesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Qid__Group__0"
    // InternalModelDsl.g:391:1: rule__Qid__Group__0 : rule__Qid__Group__0__Impl rule__Qid__Group__1 ;
    public final void rule__Qid__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:395:1: ( rule__Qid__Group__0__Impl rule__Qid__Group__1 )
            // InternalModelDsl.g:396:2: rule__Qid__Group__0__Impl rule__Qid__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Qid__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Qid__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group__0"


    // $ANTLR start "rule__Qid__Group__0__Impl"
    // InternalModelDsl.g:403:1: rule__Qid__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__Qid__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:407:1: ( ( RULE_ID ) )
            // InternalModelDsl.g:408:1: ( RULE_ID )
            {
            // InternalModelDsl.g:408:1: ( RULE_ID )
            // InternalModelDsl.g:409:2: RULE_ID
            {
             before(grammarAccess.getQidAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQidAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group__0__Impl"


    // $ANTLR start "rule__Qid__Group__1"
    // InternalModelDsl.g:418:1: rule__Qid__Group__1 : rule__Qid__Group__1__Impl ;
    public final void rule__Qid__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:422:1: ( rule__Qid__Group__1__Impl )
            // InternalModelDsl.g:423:2: rule__Qid__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Qid__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group__1"


    // $ANTLR start "rule__Qid__Group__1__Impl"
    // InternalModelDsl.g:429:1: rule__Qid__Group__1__Impl : ( ( rule__Qid__Group_1__0 )* ) ;
    public final void rule__Qid__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:433:1: ( ( ( rule__Qid__Group_1__0 )* ) )
            // InternalModelDsl.g:434:1: ( ( rule__Qid__Group_1__0 )* )
            {
            // InternalModelDsl.g:434:1: ( ( rule__Qid__Group_1__0 )* )
            // InternalModelDsl.g:435:2: ( rule__Qid__Group_1__0 )*
            {
             before(grammarAccess.getQidAccess().getGroup_1()); 
            // InternalModelDsl.g:436:2: ( rule__Qid__Group_1__0 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalModelDsl.g:436:3: rule__Qid__Group_1__0
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Qid__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getQidAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group__1__Impl"


    // $ANTLR start "rule__Qid__Group_1__0"
    // InternalModelDsl.g:445:1: rule__Qid__Group_1__0 : rule__Qid__Group_1__0__Impl rule__Qid__Group_1__1 ;
    public final void rule__Qid__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:449:1: ( rule__Qid__Group_1__0__Impl rule__Qid__Group_1__1 )
            // InternalModelDsl.g:450:2: rule__Qid__Group_1__0__Impl rule__Qid__Group_1__1
            {
            pushFollow(FOLLOW_3);
            rule__Qid__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Qid__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group_1__0"


    // $ANTLR start "rule__Qid__Group_1__0__Impl"
    // InternalModelDsl.g:457:1: rule__Qid__Group_1__0__Impl : ( '.' ) ;
    public final void rule__Qid__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:461:1: ( ( '.' ) )
            // InternalModelDsl.g:462:1: ( '.' )
            {
            // InternalModelDsl.g:462:1: ( '.' )
            // InternalModelDsl.g:463:2: '.'
            {
             before(grammarAccess.getQidAccess().getFullStopKeyword_1_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getQidAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group_1__0__Impl"


    // $ANTLR start "rule__Qid__Group_1__1"
    // InternalModelDsl.g:472:1: rule__Qid__Group_1__1 : rule__Qid__Group_1__1__Impl ;
    public final void rule__Qid__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:476:1: ( rule__Qid__Group_1__1__Impl )
            // InternalModelDsl.g:477:2: rule__Qid__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Qid__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group_1__1"


    // $ANTLR start "rule__Qid__Group_1__1__Impl"
    // InternalModelDsl.g:483:1: rule__Qid__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__Qid__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:487:1: ( ( RULE_ID ) )
            // InternalModelDsl.g:488:1: ( RULE_ID )
            {
            // InternalModelDsl.g:488:1: ( RULE_ID )
            // InternalModelDsl.g:489:2: RULE_ID
            {
             before(grammarAccess.getQidAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQidAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Qid__Group_1__1__Impl"


    // $ANTLR start "rule__ClassDecl__Group__0"
    // InternalModelDsl.g:499:1: rule__ClassDecl__Group__0 : rule__ClassDecl__Group__0__Impl rule__ClassDecl__Group__1 ;
    public final void rule__ClassDecl__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:503:1: ( rule__ClassDecl__Group__0__Impl rule__ClassDecl__Group__1 )
            // InternalModelDsl.g:504:2: rule__ClassDecl__Group__0__Impl rule__ClassDecl__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ClassDecl__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDecl__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__0"


    // $ANTLR start "rule__ClassDecl__Group__0__Impl"
    // InternalModelDsl.g:511:1: rule__ClassDecl__Group__0__Impl : ( 'class' ) ;
    public final void rule__ClassDecl__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:515:1: ( ( 'class' ) )
            // InternalModelDsl.g:516:1: ( 'class' )
            {
            // InternalModelDsl.g:516:1: ( 'class' )
            // InternalModelDsl.g:517:2: 'class'
            {
             before(grammarAccess.getClassDeclAccess().getClassKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getClassDeclAccess().getClassKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__0__Impl"


    // $ANTLR start "rule__ClassDecl__Group__1"
    // InternalModelDsl.g:526:1: rule__ClassDecl__Group__1 : rule__ClassDecl__Group__1__Impl rule__ClassDecl__Group__2 ;
    public final void rule__ClassDecl__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:530:1: ( rule__ClassDecl__Group__1__Impl rule__ClassDecl__Group__2 )
            // InternalModelDsl.g:531:2: rule__ClassDecl__Group__1__Impl rule__ClassDecl__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__ClassDecl__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDecl__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__1"


    // $ANTLR start "rule__ClassDecl__Group__1__Impl"
    // InternalModelDsl.g:538:1: rule__ClassDecl__Group__1__Impl : ( ( rule__ClassDecl__NameAssignment_1 ) ) ;
    public final void rule__ClassDecl__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:542:1: ( ( ( rule__ClassDecl__NameAssignment_1 ) ) )
            // InternalModelDsl.g:543:1: ( ( rule__ClassDecl__NameAssignment_1 ) )
            {
            // InternalModelDsl.g:543:1: ( ( rule__ClassDecl__NameAssignment_1 ) )
            // InternalModelDsl.g:544:2: ( rule__ClassDecl__NameAssignment_1 )
            {
             before(grammarAccess.getClassDeclAccess().getNameAssignment_1()); 
            // InternalModelDsl.g:545:2: ( rule__ClassDecl__NameAssignment_1 )
            // InternalModelDsl.g:545:3: rule__ClassDecl__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ClassDecl__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getClassDeclAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__1__Impl"


    // $ANTLR start "rule__ClassDecl__Group__2"
    // InternalModelDsl.g:553:1: rule__ClassDecl__Group__2 : rule__ClassDecl__Group__2__Impl rule__ClassDecl__Group__3 ;
    public final void rule__ClassDecl__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:557:1: ( rule__ClassDecl__Group__2__Impl rule__ClassDecl__Group__3 )
            // InternalModelDsl.g:558:2: rule__ClassDecl__Group__2__Impl rule__ClassDecl__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__ClassDecl__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDecl__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__2"


    // $ANTLR start "rule__ClassDecl__Group__2__Impl"
    // InternalModelDsl.g:565:1: rule__ClassDecl__Group__2__Impl : ( '{' ) ;
    public final void rule__ClassDecl__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:569:1: ( ( '{' ) )
            // InternalModelDsl.g:570:1: ( '{' )
            {
            // InternalModelDsl.g:570:1: ( '{' )
            // InternalModelDsl.g:571:2: '{'
            {
             before(grammarAccess.getClassDeclAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getClassDeclAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__2__Impl"


    // $ANTLR start "rule__ClassDecl__Group__3"
    // InternalModelDsl.g:580:1: rule__ClassDecl__Group__3 : rule__ClassDecl__Group__3__Impl rule__ClassDecl__Group__4 ;
    public final void rule__ClassDecl__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:584:1: ( rule__ClassDecl__Group__3__Impl rule__ClassDecl__Group__4 )
            // InternalModelDsl.g:585:2: rule__ClassDecl__Group__3__Impl rule__ClassDecl__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__ClassDecl__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ClassDecl__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__3"


    // $ANTLR start "rule__ClassDecl__Group__3__Impl"
    // InternalModelDsl.g:592:1: rule__ClassDecl__Group__3__Impl : ( ( rule__ClassDecl__PropertiesAssignment_3 )* ) ;
    public final void rule__ClassDecl__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:596:1: ( ( ( rule__ClassDecl__PropertiesAssignment_3 )* ) )
            // InternalModelDsl.g:597:1: ( ( rule__ClassDecl__PropertiesAssignment_3 )* )
            {
            // InternalModelDsl.g:597:1: ( ( rule__ClassDecl__PropertiesAssignment_3 )* )
            // InternalModelDsl.g:598:2: ( rule__ClassDecl__PropertiesAssignment_3 )*
            {
             before(grammarAccess.getClassDeclAccess().getPropertiesAssignment_3()); 
            // InternalModelDsl.g:599:2: ( rule__ClassDecl__PropertiesAssignment_3 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_ID) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalModelDsl.g:599:3: rule__ClassDecl__PropertiesAssignment_3
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__ClassDecl__PropertiesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getClassDeclAccess().getPropertiesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__3__Impl"


    // $ANTLR start "rule__ClassDecl__Group__4"
    // InternalModelDsl.g:607:1: rule__ClassDecl__Group__4 : rule__ClassDecl__Group__4__Impl ;
    public final void rule__ClassDecl__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:611:1: ( rule__ClassDecl__Group__4__Impl )
            // InternalModelDsl.g:612:2: rule__ClassDecl__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ClassDecl__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__4"


    // $ANTLR start "rule__ClassDecl__Group__4__Impl"
    // InternalModelDsl.g:618:1: rule__ClassDecl__Group__4__Impl : ( '}' ) ;
    public final void rule__ClassDecl__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:622:1: ( ( '}' ) )
            // InternalModelDsl.g:623:1: ( '}' )
            {
            // InternalModelDsl.g:623:1: ( '}' )
            // InternalModelDsl.g:624:2: '}'
            {
             before(grammarAccess.getClassDeclAccess().getRightCurlyBracketKeyword_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getClassDeclAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__Group__4__Impl"


    // $ANTLR start "rule__PropertyDecl__Group__0"
    // InternalModelDsl.g:634:1: rule__PropertyDecl__Group__0 : rule__PropertyDecl__Group__0__Impl rule__PropertyDecl__Group__1 ;
    public final void rule__PropertyDecl__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:638:1: ( rule__PropertyDecl__Group__0__Impl rule__PropertyDecl__Group__1 )
            // InternalModelDsl.g:639:2: rule__PropertyDecl__Group__0__Impl rule__PropertyDecl__Group__1
            {
            pushFollow(FOLLOW_11);
            rule__PropertyDecl__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PropertyDecl__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__Group__0"


    // $ANTLR start "rule__PropertyDecl__Group__0__Impl"
    // InternalModelDsl.g:646:1: rule__PropertyDecl__Group__0__Impl : ( ( rule__PropertyDecl__NameAssignment_0 ) ) ;
    public final void rule__PropertyDecl__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:650:1: ( ( ( rule__PropertyDecl__NameAssignment_0 ) ) )
            // InternalModelDsl.g:651:1: ( ( rule__PropertyDecl__NameAssignment_0 ) )
            {
            // InternalModelDsl.g:651:1: ( ( rule__PropertyDecl__NameAssignment_0 ) )
            // InternalModelDsl.g:652:2: ( rule__PropertyDecl__NameAssignment_0 )
            {
             before(grammarAccess.getPropertyDeclAccess().getNameAssignment_0()); 
            // InternalModelDsl.g:653:2: ( rule__PropertyDecl__NameAssignment_0 )
            // InternalModelDsl.g:653:3: rule__PropertyDecl__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__PropertyDecl__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyDeclAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__Group__0__Impl"


    // $ANTLR start "rule__PropertyDecl__Group__1"
    // InternalModelDsl.g:661:1: rule__PropertyDecl__Group__1 : rule__PropertyDecl__Group__1__Impl rule__PropertyDecl__Group__2 ;
    public final void rule__PropertyDecl__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:665:1: ( rule__PropertyDecl__Group__1__Impl rule__PropertyDecl__Group__2 )
            // InternalModelDsl.g:666:2: rule__PropertyDecl__Group__1__Impl rule__PropertyDecl__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__PropertyDecl__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PropertyDecl__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__Group__1"


    // $ANTLR start "rule__PropertyDecl__Group__1__Impl"
    // InternalModelDsl.g:673:1: rule__PropertyDecl__Group__1__Impl : ( ':' ) ;
    public final void rule__PropertyDecl__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:677:1: ( ( ':' ) )
            // InternalModelDsl.g:678:1: ( ':' )
            {
            // InternalModelDsl.g:678:1: ( ':' )
            // InternalModelDsl.g:679:2: ':'
            {
             before(grammarAccess.getPropertyDeclAccess().getColonKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getPropertyDeclAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__Group__1__Impl"


    // $ANTLR start "rule__PropertyDecl__Group__2"
    // InternalModelDsl.g:688:1: rule__PropertyDecl__Group__2 : rule__PropertyDecl__Group__2__Impl ;
    public final void rule__PropertyDecl__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:692:1: ( rule__PropertyDecl__Group__2__Impl )
            // InternalModelDsl.g:693:2: rule__PropertyDecl__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PropertyDecl__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__Group__2"


    // $ANTLR start "rule__PropertyDecl__Group__2__Impl"
    // InternalModelDsl.g:699:1: rule__PropertyDecl__Group__2__Impl : ( ( rule__PropertyDecl__TypeAssignment_2 ) ) ;
    public final void rule__PropertyDecl__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:703:1: ( ( ( rule__PropertyDecl__TypeAssignment_2 ) ) )
            // InternalModelDsl.g:704:1: ( ( rule__PropertyDecl__TypeAssignment_2 ) )
            {
            // InternalModelDsl.g:704:1: ( ( rule__PropertyDecl__TypeAssignment_2 ) )
            // InternalModelDsl.g:705:2: ( rule__PropertyDecl__TypeAssignment_2 )
            {
             before(grammarAccess.getPropertyDeclAccess().getTypeAssignment_2()); 
            // InternalModelDsl.g:706:2: ( rule__PropertyDecl__TypeAssignment_2 )
            // InternalModelDsl.g:706:3: rule__PropertyDecl__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__PropertyDecl__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getPropertyDeclAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__Group__2__Impl"


    // $ANTLR start "rule__IntType__Group__0"
    // InternalModelDsl.g:715:1: rule__IntType__Group__0 : rule__IntType__Group__0__Impl rule__IntType__Group__1 ;
    public final void rule__IntType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:719:1: ( rule__IntType__Group__0__Impl rule__IntType__Group__1 )
            // InternalModelDsl.g:720:2: rule__IntType__Group__0__Impl rule__IntType__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__IntType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IntType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntType__Group__0"


    // $ANTLR start "rule__IntType__Group__0__Impl"
    // InternalModelDsl.g:727:1: rule__IntType__Group__0__Impl : ( () ) ;
    public final void rule__IntType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:731:1: ( ( () ) )
            // InternalModelDsl.g:732:1: ( () )
            {
            // InternalModelDsl.g:732:1: ( () )
            // InternalModelDsl.g:733:2: ()
            {
             before(grammarAccess.getIntTypeAccess().getIntTypeAction_0()); 
            // InternalModelDsl.g:734:2: ()
            // InternalModelDsl.g:734:3: 
            {
            }

             after(grammarAccess.getIntTypeAccess().getIntTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntType__Group__0__Impl"


    // $ANTLR start "rule__IntType__Group__1"
    // InternalModelDsl.g:742:1: rule__IntType__Group__1 : rule__IntType__Group__1__Impl ;
    public final void rule__IntType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:746:1: ( rule__IntType__Group__1__Impl )
            // InternalModelDsl.g:747:2: rule__IntType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IntType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntType__Group__1"


    // $ANTLR start "rule__IntType__Group__1__Impl"
    // InternalModelDsl.g:753:1: rule__IntType__Group__1__Impl : ( 'int' ) ;
    public final void rule__IntType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:757:1: ( ( 'int' ) )
            // InternalModelDsl.g:758:1: ( 'int' )
            {
            // InternalModelDsl.g:758:1: ( 'int' )
            // InternalModelDsl.g:759:2: 'int'
            {
             before(grammarAccess.getIntTypeAccess().getIntKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getIntTypeAccess().getIntKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntType__Group__1__Impl"


    // $ANTLR start "rule__StringType__Group__0"
    // InternalModelDsl.g:769:1: rule__StringType__Group__0 : rule__StringType__Group__0__Impl rule__StringType__Group__1 ;
    public final void rule__StringType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:773:1: ( rule__StringType__Group__0__Impl rule__StringType__Group__1 )
            // InternalModelDsl.g:774:2: rule__StringType__Group__0__Impl rule__StringType__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__StringType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StringType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringType__Group__0"


    // $ANTLR start "rule__StringType__Group__0__Impl"
    // InternalModelDsl.g:781:1: rule__StringType__Group__0__Impl : ( () ) ;
    public final void rule__StringType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:785:1: ( ( () ) )
            // InternalModelDsl.g:786:1: ( () )
            {
            // InternalModelDsl.g:786:1: ( () )
            // InternalModelDsl.g:787:2: ()
            {
             before(grammarAccess.getStringTypeAccess().getStringTypeAction_0()); 
            // InternalModelDsl.g:788:2: ()
            // InternalModelDsl.g:788:3: 
            {
            }

             after(grammarAccess.getStringTypeAccess().getStringTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringType__Group__0__Impl"


    // $ANTLR start "rule__StringType__Group__1"
    // InternalModelDsl.g:796:1: rule__StringType__Group__1 : rule__StringType__Group__1__Impl ;
    public final void rule__StringType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:800:1: ( rule__StringType__Group__1__Impl )
            // InternalModelDsl.g:801:2: rule__StringType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StringType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringType__Group__1"


    // $ANTLR start "rule__StringType__Group__1__Impl"
    // InternalModelDsl.g:807:1: rule__StringType__Group__1__Impl : ( 'string' ) ;
    public final void rule__StringType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:811:1: ( ( 'string' ) )
            // InternalModelDsl.g:812:1: ( 'string' )
            {
            // InternalModelDsl.g:812:1: ( 'string' )
            // InternalModelDsl.g:813:2: 'string'
            {
             before(grammarAccess.getStringTypeAccess().getStringKeyword_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getStringTypeAccess().getStringKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringType__Group__1__Impl"


    // $ANTLR start "rule__ListType__Group__0"
    // InternalModelDsl.g:823:1: rule__ListType__Group__0 : rule__ListType__Group__0__Impl rule__ListType__Group__1 ;
    public final void rule__ListType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:827:1: ( rule__ListType__Group__0__Impl rule__ListType__Group__1 )
            // InternalModelDsl.g:828:2: rule__ListType__Group__0__Impl rule__ListType__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__ListType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ListType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListType__Group__0"


    // $ANTLR start "rule__ListType__Group__0__Impl"
    // InternalModelDsl.g:835:1: rule__ListType__Group__0__Impl : ( 'list' ) ;
    public final void rule__ListType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:839:1: ( ( 'list' ) )
            // InternalModelDsl.g:840:1: ( 'list' )
            {
            // InternalModelDsl.g:840:1: ( 'list' )
            // InternalModelDsl.g:841:2: 'list'
            {
             before(grammarAccess.getListTypeAccess().getListKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getListTypeAccess().getListKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListType__Group__0__Impl"


    // $ANTLR start "rule__ListType__Group__1"
    // InternalModelDsl.g:850:1: rule__ListType__Group__1 : rule__ListType__Group__1__Impl rule__ListType__Group__2 ;
    public final void rule__ListType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:854:1: ( rule__ListType__Group__1__Impl rule__ListType__Group__2 )
            // InternalModelDsl.g:855:2: rule__ListType__Group__1__Impl rule__ListType__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__ListType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ListType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListType__Group__1"


    // $ANTLR start "rule__ListType__Group__1__Impl"
    // InternalModelDsl.g:862:1: rule__ListType__Group__1__Impl : ( 'of' ) ;
    public final void rule__ListType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:866:1: ( ( 'of' ) )
            // InternalModelDsl.g:867:1: ( 'of' )
            {
            // InternalModelDsl.g:867:1: ( 'of' )
            // InternalModelDsl.g:868:2: 'of'
            {
             before(grammarAccess.getListTypeAccess().getOfKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getListTypeAccess().getOfKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListType__Group__1__Impl"


    // $ANTLR start "rule__ListType__Group__2"
    // InternalModelDsl.g:877:1: rule__ListType__Group__2 : rule__ListType__Group__2__Impl ;
    public final void rule__ListType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:881:1: ( rule__ListType__Group__2__Impl )
            // InternalModelDsl.g:882:2: rule__ListType__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ListType__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListType__Group__2"


    // $ANTLR start "rule__ListType__Group__2__Impl"
    // InternalModelDsl.g:888:1: rule__ListType__Group__2__Impl : ( ( rule__ListType__BaseTypeAssignment_2 ) ) ;
    public final void rule__ListType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:892:1: ( ( ( rule__ListType__BaseTypeAssignment_2 ) ) )
            // InternalModelDsl.g:893:1: ( ( rule__ListType__BaseTypeAssignment_2 ) )
            {
            // InternalModelDsl.g:893:1: ( ( rule__ListType__BaseTypeAssignment_2 ) )
            // InternalModelDsl.g:894:2: ( rule__ListType__BaseTypeAssignment_2 )
            {
             before(grammarAccess.getListTypeAccess().getBaseTypeAssignment_2()); 
            // InternalModelDsl.g:895:2: ( rule__ListType__BaseTypeAssignment_2 )
            // InternalModelDsl.g:895:3: rule__ListType__BaseTypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ListType__BaseTypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getListTypeAccess().getBaseTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListType__Group__2__Impl"


    // $ANTLR start "rule__Model__PkgAssignment_1"
    // InternalModelDsl.g:904:1: rule__Model__PkgAssignment_1 : ( ruleqid ) ;
    public final void rule__Model__PkgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:908:1: ( ( ruleqid ) )
            // InternalModelDsl.g:909:2: ( ruleqid )
            {
            // InternalModelDsl.g:909:2: ( ruleqid )
            // InternalModelDsl.g:910:3: ruleqid
            {
             before(grammarAccess.getModelAccess().getPkgQidParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleqid();

            state._fsp--;

             after(grammarAccess.getModelAccess().getPkgQidParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__PkgAssignment_1"


    // $ANTLR start "rule__Model__ClassesAssignment_2"
    // InternalModelDsl.g:919:1: rule__Model__ClassesAssignment_2 : ( ruleClassDecl ) ;
    public final void rule__Model__ClassesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:923:1: ( ( ruleClassDecl ) )
            // InternalModelDsl.g:924:2: ( ruleClassDecl )
            {
            // InternalModelDsl.g:924:2: ( ruleClassDecl )
            // InternalModelDsl.g:925:3: ruleClassDecl
            {
             before(grammarAccess.getModelAccess().getClassesClassDeclParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleClassDecl();

            state._fsp--;

             after(grammarAccess.getModelAccess().getClassesClassDeclParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__ClassesAssignment_2"


    // $ANTLR start "rule__ClassDecl__NameAssignment_1"
    // InternalModelDsl.g:934:1: rule__ClassDecl__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__ClassDecl__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:938:1: ( ( RULE_ID ) )
            // InternalModelDsl.g:939:2: ( RULE_ID )
            {
            // InternalModelDsl.g:939:2: ( RULE_ID )
            // InternalModelDsl.g:940:3: RULE_ID
            {
             before(grammarAccess.getClassDeclAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getClassDeclAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__NameAssignment_1"


    // $ANTLR start "rule__ClassDecl__PropertiesAssignment_3"
    // InternalModelDsl.g:949:1: rule__ClassDecl__PropertiesAssignment_3 : ( rulePropertyDecl ) ;
    public final void rule__ClassDecl__PropertiesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:953:1: ( ( rulePropertyDecl ) )
            // InternalModelDsl.g:954:2: ( rulePropertyDecl )
            {
            // InternalModelDsl.g:954:2: ( rulePropertyDecl )
            // InternalModelDsl.g:955:3: rulePropertyDecl
            {
             before(grammarAccess.getClassDeclAccess().getPropertiesPropertyDeclParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            rulePropertyDecl();

            state._fsp--;

             after(grammarAccess.getClassDeclAccess().getPropertiesPropertyDeclParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ClassDecl__PropertiesAssignment_3"


    // $ANTLR start "rule__PropertyDecl__NameAssignment_0"
    // InternalModelDsl.g:964:1: rule__PropertyDecl__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__PropertyDecl__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:968:1: ( ( RULE_ID ) )
            // InternalModelDsl.g:969:2: ( RULE_ID )
            {
            // InternalModelDsl.g:969:2: ( RULE_ID )
            // InternalModelDsl.g:970:3: RULE_ID
            {
             before(grammarAccess.getPropertyDeclAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPropertyDeclAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__NameAssignment_0"


    // $ANTLR start "rule__PropertyDecl__TypeAssignment_2"
    // InternalModelDsl.g:979:1: rule__PropertyDecl__TypeAssignment_2 : ( ruleType ) ;
    public final void rule__PropertyDecl__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:983:1: ( ( ruleType ) )
            // InternalModelDsl.g:984:2: ( ruleType )
            {
            // InternalModelDsl.g:984:2: ( ruleType )
            // InternalModelDsl.g:985:3: ruleType
            {
             before(grammarAccess.getPropertyDeclAccess().getTypeTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getPropertyDeclAccess().getTypeTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PropertyDecl__TypeAssignment_2"


    // $ANTLR start "rule__ListType__BaseTypeAssignment_2"
    // InternalModelDsl.g:994:1: rule__ListType__BaseTypeAssignment_2 : ( ruleType ) ;
    public final void rule__ListType__BaseTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:998:1: ( ( ruleType ) )
            // InternalModelDsl.g:999:2: ( ruleType )
            {
            // InternalModelDsl.g:999:2: ( ruleType )
            // InternalModelDsl.g:1000:3: ruleType
            {
             before(grammarAccess.getListTypeAccess().getBaseTypeTypeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getListTypeAccess().getBaseTypeTypeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ListType__BaseTypeAssignment_2"


    // $ANTLR start "rule__RefType__RefAssignment"
    // InternalModelDsl.g:1009:1: rule__RefType__RefAssignment : ( ( RULE_ID ) ) ;
    public final void rule__RefType__RefAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalModelDsl.g:1013:1: ( ( ( RULE_ID ) ) )
            // InternalModelDsl.g:1014:2: ( ( RULE_ID ) )
            {
            // InternalModelDsl.g:1014:2: ( ( RULE_ID ) )
            // InternalModelDsl.g:1015:3: ( RULE_ID )
            {
             before(grammarAccess.getRefTypeAccess().getRefClassDeclCrossReference_0()); 
            // InternalModelDsl.g:1016:3: ( RULE_ID )
            // InternalModelDsl.g:1017:4: RULE_ID
            {
             before(grammarAccess.getRefTypeAccess().getRefClassDeclIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRefTypeAccess().getRefClassDeclIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getRefTypeAccess().getRefClassDeclCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RefType__RefAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000E0010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000100000L});

}