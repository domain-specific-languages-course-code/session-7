package com.javadude.model.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.javadude.model.services.ModelDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalModelDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "'.'", "'class'", "'{'", "'}'", "':'", "'int'", "'string'", "'list'", "'of'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalModelDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalModelDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalModelDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalModelDsl.g"; }



     	private ModelDslGrammarAccess grammarAccess;

        public InternalModelDslParser(TokenStream input, ModelDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected ModelDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalModelDsl.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalModelDsl.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalModelDsl.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalModelDsl.g:71:1: ruleModel returns [EObject current=null] : (otherlv_0= 'package' ( (lv_pkg_1_0= ruleqid ) ) ( (lv_classes_2_0= ruleClassDecl ) )* ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_pkg_1_0 = null;

        EObject lv_classes_2_0 = null;



        	enterRule();

        try {
            // InternalModelDsl.g:77:2: ( (otherlv_0= 'package' ( (lv_pkg_1_0= ruleqid ) ) ( (lv_classes_2_0= ruleClassDecl ) )* ) )
            // InternalModelDsl.g:78:2: (otherlv_0= 'package' ( (lv_pkg_1_0= ruleqid ) ) ( (lv_classes_2_0= ruleClassDecl ) )* )
            {
            // InternalModelDsl.g:78:2: (otherlv_0= 'package' ( (lv_pkg_1_0= ruleqid ) ) ( (lv_classes_2_0= ruleClassDecl ) )* )
            // InternalModelDsl.g:79:3: otherlv_0= 'package' ( (lv_pkg_1_0= ruleqid ) ) ( (lv_classes_2_0= ruleClassDecl ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getModelAccess().getPackageKeyword_0());
            		
            // InternalModelDsl.g:83:3: ( (lv_pkg_1_0= ruleqid ) )
            // InternalModelDsl.g:84:4: (lv_pkg_1_0= ruleqid )
            {
            // InternalModelDsl.g:84:4: (lv_pkg_1_0= ruleqid )
            // InternalModelDsl.g:85:5: lv_pkg_1_0= ruleqid
            {

            					newCompositeNode(grammarAccess.getModelAccess().getPkgQidParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_pkg_1_0=ruleqid();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getModelRule());
            					}
            					set(
            						current,
            						"pkg",
            						lv_pkg_1_0,
            						"com.javadude.model.ModelDsl.qid");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalModelDsl.g:102:3: ( (lv_classes_2_0= ruleClassDecl ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalModelDsl.g:103:4: (lv_classes_2_0= ruleClassDecl )
            	    {
            	    // InternalModelDsl.g:103:4: (lv_classes_2_0= ruleClassDecl )
            	    // InternalModelDsl.g:104:5: lv_classes_2_0= ruleClassDecl
            	    {

            	    					newCompositeNode(grammarAccess.getModelAccess().getClassesClassDeclParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_classes_2_0=ruleClassDecl();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"classes",
            	    						lv_classes_2_0,
            	    						"com.javadude.model.ModelDsl.ClassDecl");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleqid"
    // InternalModelDsl.g:125:1: entryRuleqid returns [String current=null] : iv_ruleqid= ruleqid EOF ;
    public final String entryRuleqid() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleqid = null;


        try {
            // InternalModelDsl.g:125:43: (iv_ruleqid= ruleqid EOF )
            // InternalModelDsl.g:126:2: iv_ruleqid= ruleqid EOF
            {
             newCompositeNode(grammarAccess.getQidRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleqid=ruleqid();

            state._fsp--;

             current =iv_ruleqid.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleqid"


    // $ANTLR start "ruleqid"
    // InternalModelDsl.g:132:1: ruleqid returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleqid() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalModelDsl.g:138:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalModelDsl.g:139:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalModelDsl.g:139:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalModelDsl.g:140:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQidAccess().getIDTerminalRuleCall_0());
            		
            // InternalModelDsl.g:147:3: (kw= '.' this_ID_2= RULE_ID )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalModelDsl.g:148:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,12,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQidAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_5); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQidAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleqid"


    // $ANTLR start "entryRuleClassDecl"
    // InternalModelDsl.g:165:1: entryRuleClassDecl returns [EObject current=null] : iv_ruleClassDecl= ruleClassDecl EOF ;
    public final EObject entryRuleClassDecl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassDecl = null;


        try {
            // InternalModelDsl.g:165:50: (iv_ruleClassDecl= ruleClassDecl EOF )
            // InternalModelDsl.g:166:2: iv_ruleClassDecl= ruleClassDecl EOF
            {
             newCompositeNode(grammarAccess.getClassDeclRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassDecl=ruleClassDecl();

            state._fsp--;

             current =iv_ruleClassDecl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassDecl"


    // $ANTLR start "ruleClassDecl"
    // InternalModelDsl.g:172:1: ruleClassDecl returns [EObject current=null] : (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= rulePropertyDecl ) )* otherlv_4= '}' ) ;
    public final EObject ruleClassDecl() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_properties_3_0 = null;



        	enterRule();

        try {
            // InternalModelDsl.g:178:2: ( (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= rulePropertyDecl ) )* otherlv_4= '}' ) )
            // InternalModelDsl.g:179:2: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= rulePropertyDecl ) )* otherlv_4= '}' )
            {
            // InternalModelDsl.g:179:2: (otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= rulePropertyDecl ) )* otherlv_4= '}' )
            // InternalModelDsl.g:180:3: otherlv_0= 'class' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= rulePropertyDecl ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getClassDeclAccess().getClassKeyword_0());
            		
            // InternalModelDsl.g:184:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalModelDsl.g:185:4: (lv_name_1_0= RULE_ID )
            {
            // InternalModelDsl.g:185:4: (lv_name_1_0= RULE_ID )
            // InternalModelDsl.g:186:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            					newLeafNode(lv_name_1_0, grammarAccess.getClassDeclAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getClassDeclRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_2, grammarAccess.getClassDeclAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalModelDsl.g:206:3: ( (lv_properties_3_0= rulePropertyDecl ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalModelDsl.g:207:4: (lv_properties_3_0= rulePropertyDecl )
            	    {
            	    // InternalModelDsl.g:207:4: (lv_properties_3_0= rulePropertyDecl )
            	    // InternalModelDsl.g:208:5: lv_properties_3_0= rulePropertyDecl
            	    {

            	    					newCompositeNode(grammarAccess.getClassDeclAccess().getPropertiesPropertyDeclParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_properties_3_0=rulePropertyDecl();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getClassDeclRule());
            	    					}
            	    					add(
            	    						current,
            	    						"properties",
            	    						lv_properties_3_0,
            	    						"com.javadude.model.ModelDsl.PropertyDecl");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_4=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getClassDeclAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassDecl"


    // $ANTLR start "entryRulePropertyDecl"
    // InternalModelDsl.g:233:1: entryRulePropertyDecl returns [EObject current=null] : iv_rulePropertyDecl= rulePropertyDecl EOF ;
    public final EObject entryRulePropertyDecl() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropertyDecl = null;


        try {
            // InternalModelDsl.g:233:53: (iv_rulePropertyDecl= rulePropertyDecl EOF )
            // InternalModelDsl.g:234:2: iv_rulePropertyDecl= rulePropertyDecl EOF
            {
             newCompositeNode(grammarAccess.getPropertyDeclRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePropertyDecl=rulePropertyDecl();

            state._fsp--;

             current =iv_rulePropertyDecl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropertyDecl"


    // $ANTLR start "rulePropertyDecl"
    // InternalModelDsl.g:240:1: rulePropertyDecl returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleType ) ) ) ;
    public final EObject rulePropertyDecl() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        EObject lv_type_2_0 = null;



        	enterRule();

        try {
            // InternalModelDsl.g:246:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleType ) ) ) )
            // InternalModelDsl.g:247:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleType ) ) )
            {
            // InternalModelDsl.g:247:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleType ) ) )
            // InternalModelDsl.g:248:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_type_2_0= ruleType ) )
            {
            // InternalModelDsl.g:248:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalModelDsl.g:249:4: (lv_name_0_0= RULE_ID )
            {
            // InternalModelDsl.g:249:4: (lv_name_0_0= RULE_ID )
            // InternalModelDsl.g:250:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_0_0, grammarAccess.getPropertyDeclAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPropertyDeclRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,16,FOLLOW_9); 

            			newLeafNode(otherlv_1, grammarAccess.getPropertyDeclAccess().getColonKeyword_1());
            		
            // InternalModelDsl.g:270:3: ( (lv_type_2_0= ruleType ) )
            // InternalModelDsl.g:271:4: (lv_type_2_0= ruleType )
            {
            // InternalModelDsl.g:271:4: (lv_type_2_0= ruleType )
            // InternalModelDsl.g:272:5: lv_type_2_0= ruleType
            {

            					newCompositeNode(grammarAccess.getPropertyDeclAccess().getTypeTypeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_type_2_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPropertyDeclRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_2_0,
            						"com.javadude.model.ModelDsl.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropertyDecl"


    // $ANTLR start "entryRuleType"
    // InternalModelDsl.g:293:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalModelDsl.g:293:45: (iv_ruleType= ruleType EOF )
            // InternalModelDsl.g:294:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalModelDsl.g:300:1: ruleType returns [EObject current=null] : (this_IntType_0= ruleIntType | this_StringType_1= ruleStringType | this_ListType_2= ruleListType | this_RefType_3= ruleRefType ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_IntType_0 = null;

        EObject this_StringType_1 = null;

        EObject this_ListType_2 = null;

        EObject this_RefType_3 = null;



        	enterRule();

        try {
            // InternalModelDsl.g:306:2: ( (this_IntType_0= ruleIntType | this_StringType_1= ruleStringType | this_ListType_2= ruleListType | this_RefType_3= ruleRefType ) )
            // InternalModelDsl.g:307:2: (this_IntType_0= ruleIntType | this_StringType_1= ruleStringType | this_ListType_2= ruleListType | this_RefType_3= ruleRefType )
            {
            // InternalModelDsl.g:307:2: (this_IntType_0= ruleIntType | this_StringType_1= ruleStringType | this_ListType_2= ruleListType | this_RefType_3= ruleRefType )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt4=1;
                }
                break;
            case 18:
                {
                alt4=2;
                }
                break;
            case 19:
                {
                alt4=3;
                }
                break;
            case RULE_ID:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalModelDsl.g:308:3: this_IntType_0= ruleIntType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getIntTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntType_0=ruleIntType();

                    state._fsp--;


                    			current = this_IntType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalModelDsl.g:317:3: this_StringType_1= ruleStringType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getStringTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringType_1=ruleStringType();

                    state._fsp--;


                    			current = this_StringType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalModelDsl.g:326:3: this_ListType_2= ruleListType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getListTypeParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_ListType_2=ruleListType();

                    state._fsp--;


                    			current = this_ListType_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalModelDsl.g:335:3: this_RefType_3= ruleRefType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getRefTypeParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_RefType_3=ruleRefType();

                    state._fsp--;


                    			current = this_RefType_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleIntType"
    // InternalModelDsl.g:347:1: entryRuleIntType returns [EObject current=null] : iv_ruleIntType= ruleIntType EOF ;
    public final EObject entryRuleIntType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntType = null;


        try {
            // InternalModelDsl.g:347:48: (iv_ruleIntType= ruleIntType EOF )
            // InternalModelDsl.g:348:2: iv_ruleIntType= ruleIntType EOF
            {
             newCompositeNode(grammarAccess.getIntTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntType=ruleIntType();

            state._fsp--;

             current =iv_ruleIntType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntType"


    // $ANTLR start "ruleIntType"
    // InternalModelDsl.g:354:1: ruleIntType returns [EObject current=null] : ( () otherlv_1= 'int' ) ;
    public final EObject ruleIntType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalModelDsl.g:360:2: ( ( () otherlv_1= 'int' ) )
            // InternalModelDsl.g:361:2: ( () otherlv_1= 'int' )
            {
            // InternalModelDsl.g:361:2: ( () otherlv_1= 'int' )
            // InternalModelDsl.g:362:3: () otherlv_1= 'int'
            {
            // InternalModelDsl.g:362:3: ()
            // InternalModelDsl.g:363:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getIntTypeAccess().getIntTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getIntTypeAccess().getIntKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntType"


    // $ANTLR start "entryRuleStringType"
    // InternalModelDsl.g:377:1: entryRuleStringType returns [EObject current=null] : iv_ruleStringType= ruleStringType EOF ;
    public final EObject entryRuleStringType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringType = null;


        try {
            // InternalModelDsl.g:377:51: (iv_ruleStringType= ruleStringType EOF )
            // InternalModelDsl.g:378:2: iv_ruleStringType= ruleStringType EOF
            {
             newCompositeNode(grammarAccess.getStringTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringType=ruleStringType();

            state._fsp--;

             current =iv_ruleStringType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringType"


    // $ANTLR start "ruleStringType"
    // InternalModelDsl.g:384:1: ruleStringType returns [EObject current=null] : ( () otherlv_1= 'string' ) ;
    public final EObject ruleStringType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalModelDsl.g:390:2: ( ( () otherlv_1= 'string' ) )
            // InternalModelDsl.g:391:2: ( () otherlv_1= 'string' )
            {
            // InternalModelDsl.g:391:2: ( () otherlv_1= 'string' )
            // InternalModelDsl.g:392:3: () otherlv_1= 'string'
            {
            // InternalModelDsl.g:392:3: ()
            // InternalModelDsl.g:393:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStringTypeAccess().getStringTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getStringTypeAccess().getStringKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringType"


    // $ANTLR start "entryRuleListType"
    // InternalModelDsl.g:407:1: entryRuleListType returns [EObject current=null] : iv_ruleListType= ruleListType EOF ;
    public final EObject entryRuleListType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleListType = null;


        try {
            // InternalModelDsl.g:407:49: (iv_ruleListType= ruleListType EOF )
            // InternalModelDsl.g:408:2: iv_ruleListType= ruleListType EOF
            {
             newCompositeNode(grammarAccess.getListTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleListType=ruleListType();

            state._fsp--;

             current =iv_ruleListType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleListType"


    // $ANTLR start "ruleListType"
    // InternalModelDsl.g:414:1: ruleListType returns [EObject current=null] : (otherlv_0= 'list' otherlv_1= 'of' ( (lv_baseType_2_0= ruleType ) ) ) ;
    public final EObject ruleListType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_baseType_2_0 = null;



        	enterRule();

        try {
            // InternalModelDsl.g:420:2: ( (otherlv_0= 'list' otherlv_1= 'of' ( (lv_baseType_2_0= ruleType ) ) ) )
            // InternalModelDsl.g:421:2: (otherlv_0= 'list' otherlv_1= 'of' ( (lv_baseType_2_0= ruleType ) ) )
            {
            // InternalModelDsl.g:421:2: (otherlv_0= 'list' otherlv_1= 'of' ( (lv_baseType_2_0= ruleType ) ) )
            // InternalModelDsl.g:422:3: otherlv_0= 'list' otherlv_1= 'of' ( (lv_baseType_2_0= ruleType ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_10); 

            			newLeafNode(otherlv_0, grammarAccess.getListTypeAccess().getListKeyword_0());
            		
            otherlv_1=(Token)match(input,20,FOLLOW_9); 

            			newLeafNode(otherlv_1, grammarAccess.getListTypeAccess().getOfKeyword_1());
            		
            // InternalModelDsl.g:430:3: ( (lv_baseType_2_0= ruleType ) )
            // InternalModelDsl.g:431:4: (lv_baseType_2_0= ruleType )
            {
            // InternalModelDsl.g:431:4: (lv_baseType_2_0= ruleType )
            // InternalModelDsl.g:432:5: lv_baseType_2_0= ruleType
            {

            					newCompositeNode(grammarAccess.getListTypeAccess().getBaseTypeTypeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_baseType_2_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getListTypeRule());
            					}
            					set(
            						current,
            						"baseType",
            						lv_baseType_2_0,
            						"com.javadude.model.ModelDsl.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleListType"


    // $ANTLR start "entryRuleRefType"
    // InternalModelDsl.g:453:1: entryRuleRefType returns [EObject current=null] : iv_ruleRefType= ruleRefType EOF ;
    public final EObject entryRuleRefType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRefType = null;


        try {
            // InternalModelDsl.g:453:48: (iv_ruleRefType= ruleRefType EOF )
            // InternalModelDsl.g:454:2: iv_ruleRefType= ruleRefType EOF
            {
             newCompositeNode(grammarAccess.getRefTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRefType=ruleRefType();

            state._fsp--;

             current =iv_ruleRefType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRefType"


    // $ANTLR start "ruleRefType"
    // InternalModelDsl.g:460:1: ruleRefType returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleRefType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalModelDsl.g:466:2: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalModelDsl.g:467:2: ( (otherlv_0= RULE_ID ) )
            {
            // InternalModelDsl.g:467:2: ( (otherlv_0= RULE_ID ) )
            // InternalModelDsl.g:468:3: (otherlv_0= RULE_ID )
            {
            // InternalModelDsl.g:468:3: (otherlv_0= RULE_ID )
            // InternalModelDsl.g:469:4: otherlv_0= RULE_ID
            {

            				if (current==null) {
            					current = createModelElement(grammarAccess.getRefTypeRule());
            				}
            			
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(otherlv_0, grammarAccess.getRefTypeAccess().getRefClassDeclCrossReference_0());
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRefType"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x00000000000E0010L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000100000L});

}