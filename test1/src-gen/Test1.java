import a.b.c.Person;
import static a.b.c.Factory.*;

import java.util.Arrays;
import java.util.List;

public class Test1 {
	@SafeVarargs
	private static <T> List<T> listOf(T... items) {
		return Arrays.asList(items);
	}
	public static void main(String[] args) {
		Person person = 
				person("Scott", 51,
						listOf("Scooter", "The Tick"),
						listOf(person("Anne", 56, listOf(), listOf(), address("11 ABC","Herdon", "VA")),
								person("Mike", 82, listOf(), listOf(), address("A", "B", "C"))),
						address("123 Sesame", "New York", "NY"));
		System.out.println(person);
		
		person.addPropertyChangeListener(evt -> System.out.println("Property " + evt.getPropertyName() + " changed"));
		
		person.setName("TTT");
	}

}
