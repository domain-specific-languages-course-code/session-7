package a.b.c;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
public class Address {
	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(propertyName, listener);
	}
	private String street;
	public void setStreet(String street) {
		String oldValue = this.street;
		this.street = street;
		pcs.firePropertyChange("street", oldValue, street);
	}
	public String getStreet() { return street; }
	private String city;
	public void setCity(String city) {
		String oldValue = this.city;
		this.city = city;
		pcs.firePropertyChange("city", oldValue, city);
	}
	public String getCity() { return city; }
	private String state;
	public void setState(String state) {
		String oldValue = this.state;
		this.state = state;
		pcs.firePropertyChange("state", oldValue, state);
	}
	public String getState() { return state; }
	
	public String toString() {
		return getClass().getSimpleName() + "[" + paramString() + "]";
	}
	public String paramString() {
		StringBuilder sb = new StringBuilder();
		sb.append("street=");
		sb.append(street);
		sb.append(", ");
		sb.append("city=");
		sb.append(city);
		sb.append(", ");
		sb.append("state=");
		sb.append(state);
		return sb.toString();
	}
}
