package a.b.c;

import java.util.List;
public class Factory {
	public static Person person(
		String name, 
		int age, 
		List<String> nicknames, 
		List<Person> friends, 
		Address address
	) {
		Person person = new Person();
		person.setName(name);
		person.setAge(age);
		person.setNicknames(nicknames);
		person.setFriends(friends);
		person.setAddress(address);
		return person;
	}
	public static Address address(
		String street, 
		String city, 
		String state
	) {
		Address address = new Address();
		address.setStreet(street);
		address.setCity(city);
		address.setState(state);
		return address;
	}
	
}
