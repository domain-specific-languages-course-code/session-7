package a.b.c;

import java.util.List;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
public class Person {
	private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(propertyName, listener);
	}
	private String name;
	public void setName(String name) {
		String oldValue = this.name;
		this.name = name;
		pcs.firePropertyChange("name", oldValue, name);
	}
	public String getName() { return name; }
	private int age;
	public void setAge(int age) {
		int oldValue = this.age;
		this.age = age;
		pcs.firePropertyChange("age", oldValue, age);
	}
	public int getAge() { return age; }
	private List<String> nicknames;
	public void setNicknames(List<String> nicknames) {
		List<String> oldValue = this.nicknames;
		this.nicknames = nicknames;
		pcs.firePropertyChange("nicknames", oldValue, nicknames);
	}
	public List<String> getNicknames() { return nicknames; }
	private List<Person> friends;
	public void setFriends(List<Person> friends) {
		List<Person> oldValue = this.friends;
		this.friends = friends;
		pcs.firePropertyChange("friends", oldValue, friends);
	}
	public List<Person> getFriends() { return friends; }
	private Address address;
	public void setAddress(Address address) {
		Address oldValue = this.address;
		this.address = address;
		pcs.firePropertyChange("address", oldValue, address);
	}
	public Address getAddress() { return address; }
	
	public String toString() {
		return getClass().getSimpleName() + "[" + paramString() + "]";
	}
	public String paramString() {
		StringBuilder sb = new StringBuilder();
		sb.append("name=");
		sb.append(name);
		sb.append(", ");
		sb.append("age=");
		sb.append(age);
		sb.append(", ");
		sb.append("nicknames=");
		sb.append(nicknames);
		sb.append(", ");
		sb.append("friends=");
		sb.append(friends);
		sb.append(", ");
		sb.append("address=");
		sb.append(address);
		return sb.toString();
	}
}
