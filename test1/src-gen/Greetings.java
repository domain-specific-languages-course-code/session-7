public class Greetings {
	public static void main(String[] args) {
		System.out.println("Hello Scott!");
		System.out.println("Hello Steve!");
		System.out.println("Hello Jello!");
		System.out.println("------");
		System.out.println("Hello Scott, Steve, Jello!");
		System.out.println("------");
		System.out.println("Hello Scott!");
		System.out.println("Hello Steve!");
		System.out.println("Hello Jello!");
	}
}
