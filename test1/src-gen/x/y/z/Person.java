package x.y.z;

import java.util.ArrayList;
import java.util.List;
import x.y.z.Address;

@SuppressWarnings("all")
public class Person {
  private String name;
  
  public String getName() {
    return this.name;
  }
  
  public void setName(final String name) {
    this.name = name;
  }
  
  private int age = 42;
  
  public int getAge() {
    return this.age;
  }
  
  public void setAge(final int age) {
    this.age = age;
  }
  
  private List<String> nicknames;
  
  public List<String> getNicknames() {
    return this.nicknames;
  }
  
  public void setNicknames(final List<String> nicknames) {
    this.nicknames = nicknames;
  }
  
  private List<Person> friends = new ArrayList<Person>();
  
  public List<Person> getFriends() {
    return this.friends;
  }
  
  public void setFriends(final List<Person> friends) {
    this.friends = friends;
  }
  
  private Address address;
  
  public Address getAddress() {
    return this.address;
  }
  
  public void setAddress(final Address address) {
    this.address = address;
  }
  
  public String toString() {
    return "hello!";
  }
}
