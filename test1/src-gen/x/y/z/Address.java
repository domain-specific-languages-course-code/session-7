package x.y.z;

@SuppressWarnings("all")
public class Address {
  private String street;
  
  public String getStreet() {
    return this.street;
  }
  
  public void setStreet(final String street) {
    this.street = street;
  }
  
  private String city;
  
  public String getCity() {
    return this.city;
  }
  
  public void setCity(final String city) {
    this.city = city;
  }
  
  private String state;
  
  public String getState() {
    return this.state;
  }
  
  public void setState(final String state) {
    this.state = state;
  }
  
  public String toString() {
    return "hello!";
  }
}
