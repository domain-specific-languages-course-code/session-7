package test1;

import a.b.c.Person;
import static a.b.c.Factory.*;

import java.util.Arrays;
import java.util.List;

public class Test2 {
	private static <T> List<T> listOf(T... values) {
		return Arrays.asList(values);
	}
	
	public static void main(String[] args) {
		Person person = person("Scott", 51, 
				listOf("Scooter", "The Tick"), 
				listOf(
						person("Anne", 56, listOf(), listOf(),
								address("11 Thomas", "Herndon", "VA")),
						person("Dan", 42, listOf(), listOf(),
								address("Some Street", "Some City", "MD"))),
				address("123 Sesame", "Ann Arbor", "MI"));
		
		System.out.println(person);
	}

}
