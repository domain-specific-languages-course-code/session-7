/**
 * generated by Xtext 2.14.0
 */
package com.javadude.infer1.validation;

import com.javadude.infer1.validation.AbstractInferDslValidator;

/**
 * This class contains custom validation rules.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
@SuppressWarnings("all")
public class InferDslValidator extends AbstractInferDslValidator {
}
